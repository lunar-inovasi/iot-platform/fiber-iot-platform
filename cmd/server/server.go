package server

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/w1lldone/fiber-iot-platform/app/models"
	"github.com/w1lldone/fiber-iot-platform/app/validation"
	"github.com/w1lldone/fiber-iot-platform/pkg/config"
	"github.com/w1lldone/fiber-iot-platform/routes"
)

// Serve serves the app
func Serve() {
	appCfg := config.AppCfg()

	err := models.New(config.DbCfg())
	if err != nil {
		log.Fatalf("could not connect to database. error: %v", err)
	}

	err = models.Migrate()
	if err != nil {
		log.Fatalf("error on migrating database. error: %v", err)
	}

	err = validation.Init()
	if err != nil {
		log.Fatalf("error initiating validator. error: %v", err)
	}

	fiberCfg := config.FiberConfig()
	app := fiber.New(fiberCfg)

	app.Use(
		logger.New(),
	)

	app.Get("/", func(c *fiber.Ctx) error {
		return c.JSON(fiber.Map{
			"status": "ok",
		})
	})

	routes.RegisterWebRoutes(app)

	// signal channel to capture system calls
	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT, syscall.SIGQUIT)

	// start shutdown goroutine
	go func() {
		// capture sigterm and other system call here
		<-sigCh
		log.Println("Shutting down server...")
		_ = app.Shutdown()
	}()

	// start http server
	serverAddr := fmt.Sprintf("%s:%d", appCfg.Host, appCfg.Port)
	err = app.Listen(serverAddr)
	if err != nil {
		log.Fatalf("can not start server. error: %v", err)
	}
}
