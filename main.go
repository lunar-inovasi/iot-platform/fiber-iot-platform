package main

import (
	"github.com/w1lldone/fiber-iot-platform/cmd/server"
	"github.com/w1lldone/fiber-iot-platform/pkg/config"
)

func main() {
	config.LoadAllConfig(".env")
	server.Serve()
}
