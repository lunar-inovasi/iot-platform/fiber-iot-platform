package controller

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/w1lldone/fiber-iot-platform/app/models"
	"github.com/w1lldone/fiber-iot-platform/app/validation"
	"golang.org/x/crypto/bcrypt"
)

type userStoreRequest struct {
	Name                 string `json:"name" validate:"required" error:"Name "`
	Email                string `json:"email" validate:"email,required"`
	Password             string `json:"password" validate:"required,min=6"`
	PasswordConfirmation string `json:"password_confirmation" validate:"eqfield=Password,required"`
	OrganizationID       uint   `json:"organization_id" validate:"required" error:"OrganizationID "`
}

func UserIndex(c *fiber.Ctx) error {
	var users []models.User
	models.DB.Find(&users)

	return c.JSON(fiber.Map{
		"data": users,
	})
}

func UserStore(c *fiber.Ctx) error {
	// Parse request
	request := &userStoreRequest{}
	err := c.BodyParser(request)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	// Validate request
	err = validation.Struct(request)
	if err != nil {
		errors := fiber.Map{}
		for _, verr := range err.(validator.ValidationErrors) {
			errors[verr.Field()] = verr.Translate(validation.Translator())
		}
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"errors": errors,
		})
	}

	var count int64
	models.DB.Model(&models.User{}).Where("email = ?", request.Email).Count(&count)
	if count > 0 {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"errors": fiber.Map{
				"email": "the email has been taken",
			},
		})
	}

	hashed, err := bcrypt.GenerateFromPassword([]byte(request.Password), bcrypt.DefaultCost)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": err,
		})
	}

	// create user with organization
	user := models.User{
		Name:           request.Name,
		Email:          request.Email,
		Password:       string(hashed),
		OrganizationID: request.OrganizationID,
	}
	models.DB.Create(&user)

	return c.JSON(fiber.Map{
		"data": user,
	})
}
