package controller

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/w1lldone/fiber-iot-platform/app/models"
	"github.com/w1lldone/fiber-iot-platform/app/validation"
)

type organizationStoreRequest struct {
	Name        string `json:"name" validate:"required" error:"Name "`
	Description string `json:"description" validate:"required"`
}

func OrganizationIndex(c *fiber.Ctx) error {
	var organizations []models.Organization
	models.DB.Preload("Users").Find(&organizations)

	return c.JSON(fiber.Map{
		"data": organizations,
	})
}

func OrganizationStore(c *fiber.Ctx) error {
	// Parse request
	request := &organizationStoreRequest{}
	err := c.BodyParser(request)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	// validate
	err = validation.Struct(request)
	if err != nil {
		errors := fiber.Map{}
		for _, verr := range err.(validator.ValidationErrors) {
			errors[verr.Field()] = verr.Translate(validation.Translator())
		}
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"errors": errors,
		})
	}

	organization := models.Organization{
		Name:        request.Name,
		Description: request.Description,
	}
	models.DB.Create(&organization)

	return c.JSON(fiber.Map{
		"data": organization,
	})
}
