package models

import "gorm.io/gorm"

// organization has many user
type Organization struct {
	gorm.Model
	Name        string `json:"name"`
	Description string `json:"description"`
	Users       []User `json:"users" gorm:"foreignKey:OrganizationID"`
}
