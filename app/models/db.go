package models

import (
	"errors"
	"strings"

	"github.com/w1lldone/fiber-iot-platform/pkg/config"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func New(config *config.DB) error {
	var err error
	var db *gorm.DB
	switch strings.ToLower(config.Driver) {
	case "mysql":
		dsn := config.Username + ":" + config.Password + "@tcp(" + config.Host + ":" + config.Port + ")/" + config.Database + "?charset=utf8mb4&collation=utf8mb4_general_ci&parseTime=True&loc=UTC"
		db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
		if err != nil {
			return err
		}
		DB = db
	default:
		return errors.New("driver is not supported")
	}

	return nil
}

func Migrate() error {
	return DB.AutoMigrate(&User{}, &Organization{})
}
