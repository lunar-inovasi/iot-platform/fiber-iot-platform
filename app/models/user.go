package models

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Name           string        `json:"name"`
	Email          string        `json:"email"`
	Password       string        `json:"-"`
	OrganizationID uint          `json:"organization_id"`
	Organization   *Organization `json:"organization"`
}
