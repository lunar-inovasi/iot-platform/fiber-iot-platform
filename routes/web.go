package routes

import (
	"github.com/gofiber/fiber/v2"
	"github.com/w1lldone/fiber-iot-platform/app/controller"
)

func RegisterWebRoutes(app *fiber.App) {
	app.Get("/users", controller.UserIndex)
	app.Post("/users", controller.UserStore)
	// organization routes
	app.Get("/organizations", controller.OrganizationIndex)
	app.Post("/organizations", controller.OrganizationStore)
}
