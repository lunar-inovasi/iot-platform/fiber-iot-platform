package config

import (
	"os"
)

type DB struct {
	Driver   string
	Host     string
	Database string
	Username string
	Password string
	Port     string
}

var db = &DB{}

func LoadDB() {
	db.Driver = os.Getenv("DB_DRIVER")
	db.Host = os.Getenv("DB_HOST")
	db.Username = os.Getenv("DB_USERNAME")
	db.Database = os.Getenv("DB_DATABASE")
	db.Username = os.Getenv("DB_USERNAME")
	db.Password = os.Getenv("DB_PASSWORD")
	db.Port = os.Getenv("DB_PORT")
}

func DbCfg() *DB {
	return db
}
