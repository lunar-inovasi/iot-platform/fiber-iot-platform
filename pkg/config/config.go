package config

import (
	"log"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
)

// LoadAllConfig reads the env and setup the configs
func LoadAllConfig(envFile string) {
	err := godotenv.Load(envFile)
	if err != nil {
		log.Fatalf("can't load env file. error %v", err)
	}

	LoadApp()
	LoadDB()
}

func FiberConfig() fiber.Config {
	return fiber.Config{
		ReadTimeout: time.Duration(AppCfg().ReadTimeout) * time.Second,
	}
}
