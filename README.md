# IoT Platform Experimental using Fiber
## Instalation
- Install Golang https://go.dev/doc/install
- Copy `.env.example` to `.env` and fill the configuration
- Serve the project
```sh
$ go run .
```
```
 ┌───────────────────────────────────────────────────┐ 
 │                   Fiber v2.51.0                   │ 
 │               http://127.0.0.1:8000               │ 
 │       (bound on host 0.0.0.0 and port 8000)       │ 
 │                                                   │ 
 │ Handlers ............. 3  Processes ........... 1 │ 
 │ Prefork ....... Disabled  PID .............. 6826 │ 
 └───────────────────────────────────────────────────┘ 
```